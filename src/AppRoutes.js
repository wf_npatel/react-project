import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import App from './App';
import Page2 from './page-2';


const AppRoutes = () => {
  return (
    <Router>
      <Switch>
        <Route path="/page2" exact component={Page2} />
        <Route path="/" exact component={App} />
        <Route render={() => <h1>Not found</h1>} />
      </Switch>
    </Router>
  );
}


export default AppRoutes;
