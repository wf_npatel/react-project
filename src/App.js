import './App.css';

import React, { useState } from 'react';
import { Link } from 'react-router-dom';

function App() {

  const [header, setHeader] = useState('default');
  const [collapsed1, setCollapsed1] = useState(true);
  const [collapsed2, setCollapsed2] = useState(true);
  const [collapsed3, setCollapsed3] = useState(true);
  const [checkbox1, setCheckbox1] = useState(true);
  const [checkbox2, setCheckbox2] = useState(true);
  const [checkbox3, setCheckbox3] = useState(true);


  return (
    <div className="main-wrapper">
      <div className="rectangle">
        <div className="rectangle-header">
          <h4 className="header-title">Customize Confirmation Pop Up</h4>
          <Link to="" onClick={(e) => e.preventDefault()} className="close-btn">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
              <path fill="#FFF"
                d="M9.772 7.962L15.2 13.39c.508.507.508 1.302 0 1.81-.507.507-1.302.507-1.81 0L7.963 9.77l-5.429 5.43c-.507.507-1.302.507-1.81 0-.507-.508-.507-1.303 0-1.81l5.43-5.43-5.43-5.428c-.507-.507-.507-1.302 0-1.81.508-.507 1.303-.507 1.81 0l5.429 5.43L13.39.722c.507-.507 1.302-.507 1.81 0 .507.508.507 1.303 0 1.81L9.77 7.962z" />
            </svg>
          </Link>
        </div>
        <div className="rectangle-body">
          <div className="inner-body-content">
            <div className="warning-ractangle">
              <div className="warning-body">
                <Link to="" onClick={(e) => e.preventDefault()} className="close-warning">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                    <path fill="#B8B8B8" d="M9.772 7.962L15.2 13.39c.508.507.508 1.302 0 1.81-.507.507-1.302.507-1.81 0L7.963 9.77l-5.429 5.43c-.507.507-1.302.507-1.81 0-.507-.508-.507-1.303 0-1.81l5.43-5.43-5.43-5.428c-.507-.507-.507-1.302 0-1.81.508-.507 1.303-.507 1.81 0l5.429 5.43L13.39.722c.507-.507 1.302-.507 1.81 0 .507.508.507 1.303 0 1.81L9.77 7.962z" />
                  </svg>
                </Link>
                <div className="caution-circle">
                  <img src={require("./assets/img/caution.svg")} alt="" />
                </div>
                <h4>Please Confirm</h4>
                <p>Are you sure you want to<br /> submit? This cannot be undone.</p>
              </div>

              <div className="warning-footer">
                <Link to="" onClick={(e) => e.preventDefault()} className="btn" type="button">No</Link>
                <button className="btn btn-yes" type="button">Yes</button>
              </div>
            </div>
          </div>
          <div className="rectangle-right-menu">
            <div className="inner-rectangle-menu">
              <ul className="top-menu">
                <li className={`${header === 'default' ? 'active' : ''}`}><Link to="" onClick={(e) => { setHeader('default'); e.preventDefault() }} className="active link" >Default</Link>
                </li>
                <li className={`${header === 'hover' ? 'active' : ''}`}><Link to="" onClick={(e) => { setHeader('hover'); e.preventDefault() }} className="link" >Hover</Link></li>
                <li className={`${header === 'clicked' ? 'active' : ''}`}><Link to="" onClick={(e) => { setHeader('clicked'); e.preventDefault() }} className="link" >Clicked</Link>
                </li>
              </ul>
              {header === 'default' ?
                <div id="Default" className="tabcontent">
                  <div className="font-reactangle menu-rectangle">
                    <div className={collapsed1 ? 'menu-header ' : 'menu-header border-bottom'}>
                      <h4 className="menu-title">Font</h4>
                      <Link to="" onClick={(e) => { setCollapsed1(!collapsed1); e.preventDefault() }} className={collapsed1 ? 'minus-sign' : 'minus-sign plus'}>
                      </Link>
                    </div>
                    {collapsed1 ?
                      <div className="menu-body" >
                        <div className="font-first-row">
                          <div className="custom-select-container">
                            <select className="custom-select">
                              <option>Lato</option>
                              <option>Times new roman</option>
                              <option>Arial</option>
                            </select>
                            <i className="fa fa-angle-down arrow-down" aria-hidden="true"></i>
                          </div>


                        </div>
                        <div className="font-second-box">
                          <div className="custom-select-container">
                            <select className="custom-select">
                              <option>Semibold</option>
                              <option>Bold</option>
                              <option>Normal</option>
                            </select>
                            <i className="fa fa-angle-down arrow-down" aria-hidden="true"></i>
                          </div>
                          <div className="custom-select-container font-size-selectBox">
                            <select className="custom-select ">
                              <option>64</option>
                              <option>34</option>
                              <option>24</option>
                              <option>12</option>
                            </select>
                            <i className="fa fa-angle-down arrow-down" aria-hidden="true"></i>
                          </div>
                        </div>
                        <div className="font-third-row">
                          <ul className="list-style-none">
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-color.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-left-align.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-center-align.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-right-align.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-left-justify.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-italic.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-underline.svg")} /></Link>
                            </li>
                          </ul>
                        </div>
                        <div className="font-fourth-row">
                          <div className="input-container">
                            <div className="inner-input">
                              <input type="text" value="96" onChange={() => console.log('object')} />
                              <span className="px-text">px</span>
                            </div>
                            <span className="input-bottom-text">Line Height</span>

                          </div>
                          <div className="input-container">
                            <div className="inner-input">
                              <input type="text" value="1" onChange={() => console.log('object')} />
                              <span className="px-text">px</span>
                            </div>
                            <span className="input-bottom-text">Character</span>
                          </div>
                        </div>
                      </div>
                      : null}
                  </div>
                  <div className="menu-rectangle">
                    <div className={collapsed2 ? 'menu-header ' : 'menu-header border-bottom'}>
                      <h4 className="menu-title">Fill</h4>
                      <Link to="" onClick={(e) => { setCollapsed2(!collapsed2); e.preventDefault() }} className={collapsed2 ? 'minus-sign' : 'minus-sign plus'} >

                      </Link>
                    </div>
                    {collapsed2 ?
                      <div className="menu-body">
                        <div className="fill-first-row">
                          <label className="check-box">
                            <input type="checkbox" checked={checkbox1} onChange={() => { setCheckbox1(!checkbox1); }} />
                            <span className="checkmark"></span>
                          </label>
                          <Link to="" onClick={(e) => e.preventDefault()} className="color-box"><img alt=" " src={require("./assets/img/noun-color.svg")} /></Link>
                          <div className="input-container">
                            <div className="inner-input">
                              <input type="text" value="000000" onChange={() => console.log('object')} />
                            </div>
                            <div className="input-bottom-text dropdown-container"> 
                              <span>HEX </span>
                              <i className="fa fa-angle-down arrow-down" aria-hidden="true"></i>
                              <ul className="dropdown-item">
                                <li><Link to="" onClick={(e) => e.preventDefault()} >RGBA</Link></li>
                                <li><Link to="" onClick={(e) => e.preventDefault()} >HSLA</Link></li>
                              </ul>
                            </div>
                          </div>
                          <div className="input-container">
                            <div className="inner-input percent-value">
                              <input type="text" value="100%" onChange={() => console.log('object')} />
                            </div>
                            <span className="input-bottom-text">Opacity</span>
                          </div>
                          <div className="circle-gradient"></div>

                        </div>
                        <div className="fill-seconf-row">
                          <div className="file-upload-container">
                            <input type="file" id="avatar" name="avatar" accept="image/png, image/jpeg" />
                            <span>Use Image</span>
                          </div>
                        </div>
                      </div>
                      : null}
                  </div>
                  <div className="menu-rectangle">
                    <div className={collapsed3 ? 'menu-header ' : 'menu-header border-bottom'}>
                      <h4 className="menu-title">Border</h4>
                      <Link to="" onClick={(e) => { setCollapsed3(!collapsed3); e.preventDefault() }} className={collapsed3 ? 'minus-sign' : 'minus-sign plus'}>
                      </Link>
                    </div>
                    {collapsed3 ?
                      <div className="menu-body">
                        <div className="fill-first-row">
                          <label className="check-box">
                            <input type="checkbox" checked={checkbox2} onChange={() => { setCheckbox2(!checkbox2) }} />
                            <span className="checkmark"></span>
                          </label>
                          <Link to="" onClick={(e) => e.preventDefault()} className="color-box"><img alt=" " src={require("./assets/img/noun-color.svg")} /></Link>
                          <div className="input-container">
                            <div className="inner-input">
                              <input type="text" value="000000" onChange={() => console.log('object')} />
                            </div>
                            <div className="input-bottom-text dropdown-container"> 
                              <span>HEX </span>
                              <i className="fa fa-angle-down arrow-down" aria-hidden="true"></i>
                              <ul className="dropdown-item">
                                <li><Link to="" onClick={(e) => e.preventDefault()} >RGBA</Link></li>
                                <li><Link to="" onClick={(e) => e.preventDefault()} >HSLA</Link></li>
                              </ul>
                            </div>
                          </div>
                          <div className="input-container">
                            <div className="inner-input percent-value">
                              <input type="text" value="100%" onChange={() => console.log('object')} />
                            </div>
                            <span className="input-bottom-text">Opacity</span>
                          </div>
                          <div className="input-container">
                            <div className="inner-input border-value">
                              <input type="text" value="1" onChange={() => console.log('object')} />
                            </div>
                          </div>

                        </div>
                      </div>
                      : null}
                  </div>
                </div> : ''
              }

              {header === 'hover' ?
              <div id="hover" className="tabcontent">
                  <div className="font-reactangle menu-rectangle">
                    <div className={collapsed1 ? 'menu-header ' : 'menu-header border-bottom'}>
                      <h4 className="menu-title">Font</h4>
                      <Link to="" onClick={(e) => { setCollapsed1(!collapsed1); e.preventDefault() }} className={collapsed1 ? 'minus-sign' : 'minus-sign plus'}>
                      </Link>
                    </div>
                    {collapsed1 ?
                      <div className="menu-body" >
                        <div className="font-first-row">
                          <div className="custom-select-container">
                            <select className="custom-select">
                              <option>Lato</option>
                              <option>Times new roman</option>
                              <option>Arial</option>
                            </select>
                            <i className="fa fa-angle-down arrow-down" aria-hidden="true"></i>
                          </div>


                        </div>
                        <div className="font-second-box">
                          <div className="custom-select-container">
                            <select className="custom-select">
                              <option>Semibold</option>
                              <option>Bold</option>
                              <option>Normal</option>
                            </select>
                            <i className="fa fa-angle-down arrow-down" aria-hidden="true"></i>
                          </div>
                          <div className="custom-select-container font-size-selectBox">
                            <select className="custom-select ">
                              <option>64</option>
                              <option>34</option>
                              <option>24</option>
                              <option>12</option>
                            </select>
                            <i className="fa fa-angle-down arrow-down" aria-hidden="true"></i>
                          </div>
                        </div>
                        <div className="font-third-row">
                          <ul className="list-style-none">
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-color.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-left-align.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-center-align.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-right-align.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-left-justify.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-italic.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-underline.svg")} /></Link>
                            </li>
                          </ul>
                        </div>
                        <div className="font-fourth-row">
                          <div className="input-container">
                            <div className="inner-input">
                              <input type="text" value="96" onChange={() => console.log('object')} />
                              <span className="px-text">px</span>
                            </div>
                            <span className="input-bottom-text">Line Height</span>

                          </div>
                          <div className="input-container">
                            <div className="inner-input">
                              <input type="text" value="1" onChange={() => console.log('object')} />
                              <span className="px-text">px</span>
                            </div>
                            <span className="input-bottom-text">Character</span>
                          </div>
                        </div>
                      </div>
                      : null}
                  </div>
                  <div className="menu-rectangle">
                    <div className={collapsed2 ? 'menu-header ' : 'menu-header border-bottom'}>
                      <h4 className="menu-title">Fill</h4>
                      <Link to="" onClick={(e) => { setCollapsed2(!collapsed2); e.preventDefault() }} className={collapsed2 ? 'minus-sign' : 'minus-sign plus'} >

                      </Link>
                    </div>
                    {collapsed2 ?
                      <div className="menu-body">
                        <div className="fill-first-row">
                          <label className="check-box">
                            <input type="checkbox" checked={checkbox1} onChange={() => { setCheckbox1(!checkbox1); }} />
                            <span className="checkmark"></span>
                          </label>
                          <Link to="" onClick={(e) => e.preventDefault()} className="color-box"><img alt=" " src={require("./assets/img/noun-color.svg")} /></Link>
                          <div className="input-container">
                            <div className="inner-input">
                              <input type="text" value="000000" onChange={() => console.log('object')} />
                            </div>
                            <div className="input-bottom-text dropdown-container"> 
                              <span>HEX </span>
                              <i className="fa fa-angle-down arrow-down" aria-hidden="true"></i>
                              <ul className="dropdown-item">
                                <li><Link to="" onClick={(e) => e.preventDefault()} >RGBA</Link></li>
                                <li><Link to="" onClick={(e) => e.preventDefault()} >HSLA</Link></li>
                              </ul>
                            </div>
                          </div>
                          <div className="input-container">
                            <div className="inner-input percent-value">
                              <input type="text" value="100%" onChange={() => console.log('object')} />
                            </div>
                            <span className="input-bottom-text">Opacity</span>
                          </div>
                          <div className="circle-gradient"></div>

                        </div>
                        <div className="fill-seconf-row">
                          <div className="file-upload-container">
                            <input type="file" id="avatar" name="avatar" accept="image/png, image/jpeg" />
                            <span>Use Image</span>
                          </div>
                        </div>
                      </div>
                      : null}
                  </div>
                  <div className="menu-rectangle">
                    <div className={collapsed3 ? 'menu-header ' : 'menu-header border-bottom'}>
                      <h4 className="menu-title">Border</h4>
                      <Link to="" onClick={(e) => { setCollapsed3(!collapsed3); e.preventDefault() }} className={collapsed3 ? 'minus-sign' : 'minus-sign plus'}>
                      </Link>
                    </div>
                    {collapsed3 ?
                      <div className="menu-body">
                        <div className="fill-first-row">
                          <label className="check-box">
                            <input type="checkbox" checked={checkbox2} onChange={() => { setCheckbox2(!checkbox2) }} />
                            <span className="checkmark"></span>
                          </label>
                          <Link to="" onClick={(e) => e.preventDefault()} className="color-box"><img alt=" " src={require("./assets/img/noun-color.svg")} /></Link>
                          <div className="input-container">
                            <div className="inner-input">
                              <input type="text" value="000000" onChange={() => console.log('object')} />
                            </div>
                            <div className="input-bottom-text dropdown-container"> 
                              <span>HEX </span>
                              <i className="fa fa-angle-down arrow-down" aria-hidden="true"></i>
                              <ul className="dropdown-item">
                                <li><Link to="" onClick={(e) => e.preventDefault()} >RGBA</Link></li>
                                <li><Link to="" onClick={(e) => e.preventDefault()} >HSLA</Link></li>
                              </ul>
                            </div>
                          </div>
                          <div className="input-container">
                            <div className="inner-input percent-value">
                              <input type="text" value="100%" onChange={() => console.log('object')} />
                            </div>
                            <span className="input-bottom-text">Opacity</span>
                          </div>
                          <div className="input-container">
                            <div className="inner-input border-value">
                              <input type="text" value="1" onChange={() => console.log('object')} />
                            </div>
                          </div>

                        </div>
                      </div>
                      : null}
                  </div>
                </div> : ''
              }


              {header === 'clicked' ?
              <div id="clicked" className="tabcontent">
                  <div className="font-reactangle menu-rectangle">
                    <div className={collapsed1 ? 'menu-header ' : 'menu-header border-bottom'}>
                      <h4 className="menu-title">Font</h4>
                      <Link to="" onClick={(e) => { setCollapsed1(!collapsed1); e.preventDefault() }} className={collapsed1 ? 'minus-sign' : 'minus-sign plus'}>
                      </Link>
                    </div>
                    {collapsed1 ?
                      <div className="menu-body" >
                        <div className="font-first-row">
                          <div className="custom-select-container">
                            <select className="custom-select">
                              <option>Lato</option>
                              <option>Times new roman</option>
                              <option>Arial</option>
                            </select>
                            <i className="fa fa-angle-down arrow-down" aria-hidden="true"></i>
                          </div>


                        </div>
                        <div className="font-second-box">
                          <div className="custom-select-container">
                            <select className="custom-select">
                              <option>Semibold</option>
                              <option>Bold</option>
                              <option>Normal</option>
                            </select>
                            <i className="fa fa-angle-down arrow-down" aria-hidden="true"></i>
                          </div>
                          <div className="custom-select-container font-size-selectBox">
                            <select className="custom-select ">
                              <option>64</option>
                              <option>34</option>
                              <option>24</option>
                              <option>12</option>
                            </select>
                            <i className="fa fa-angle-down arrow-down" aria-hidden="true"></i>
                          </div>
                        </div>
                        <div className="font-third-row">
                          <ul className="list-style-none">
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-color.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-left-align.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-center-align.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-right-align.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-left-justify.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-italic.svg")} /></Link>
                            </li>
                            <li>
                              <Link to="" onClick={(e) => e.preventDefault()}><img alt=" " src={require("./assets/img/noun-underline.svg")} /></Link>
                            </li>
                          </ul>
                        </div>
                        <div className="font-fourth-row">
                          <div className="input-container">
                            <div className="inner-input">
                              <input type="text" value="96" onChange={() => console.log('object')} />
                              <span className="px-text">px</span>
                            </div>
                            <span className="input-bottom-text">Line Height</span>

                          </div>
                          <div className="input-container">
                            <div className="inner-input">
                              <input type="text" value="1" onChange={() => console.log('object')} />
                              <span className="px-text">px</span>
                            </div>
                            <span className="input-bottom-text">Character</span>
                          </div>
                        </div>
                      </div>
                      : null}
                  </div>
                  <div className="menu-rectangle">
                    <div className={collapsed2 ? 'menu-header ' : 'menu-header border-bottom'}>
                      <h4 className="menu-title">Fill</h4>
                      <Link to="" onClick={(e) => { setCollapsed2(!collapsed2); e.preventDefault() }} className={collapsed2 ? 'minus-sign' : 'minus-sign plus'} >

                      </Link>
                    </div>
                    {collapsed2 ?
                      <div className="menu-body">
                        <div className="fill-first-row">
                          <label className="check-box">
                            <input type="checkbox" checked={checkbox1} onChange={() => { setCheckbox1(!checkbox1); }} />
                            <span className="checkmark"></span>
                          </label>
                          <Link to="" onClick={(e) => e.preventDefault()} className="color-box"><img alt=" " src={require("./assets/img/noun-color.svg")} /></Link>
                          <div className="input-container">
                            <div className="inner-input">
                              <input type="text" value="000000" onChange={() => console.log('object')} />
                            </div>
                            <div className="input-bottom-text dropdown-container"> 
                              <span>HEX </span>
                              <i className="fa fa-angle-down arrow-down" aria-hidden="true"></i>
                              <ul className="dropdown-item">
                                <li><Link to="" onClick={(e) => e.preventDefault()} >RGBA</Link></li>
                                <li><Link to="" onClick={(e) => e.preventDefault()} >HSLA</Link></li>
                              </ul>
                            </div>
                          </div>
                          <div className="input-container">
                            <div className="inner-input percent-value">
                              <input type="text" value="100%" onChange={() => console.log('object')} />
                            </div>
                            <span className="input-bottom-text">Opacity</span>
                          </div>
                          <div className="circle-gradient"></div>

                        </div>
                        <div className="fill-seconf-row">
                          <div className="file-upload-container">
                            <input type="file" id="avatar" name="avatar" accept="image/png, image/jpeg" />
                            <span>Use Image</span>
                          </div>
                        </div>
                      </div>
                      : null}
                  </div>
                  <div className="menu-rectangle">
                    <div className={collapsed3 ? 'menu-header ' : 'menu-header border-bottom'}>
                      <h4 className="menu-title">Border</h4>
                      <Link to="" onClick={(e) => { setCollapsed3(!collapsed3); e.preventDefault() }} className={collapsed3 ? 'minus-sign' : 'minus-sign plus'}>
                      </Link>
                    </div>
                    {collapsed3 ?
                      <div className="menu-body">
                        <div className="fill-first-row">
                          <label className="check-box">
                            <input type="checkbox" checked={checkbox2} onChange={() => { setCheckbox2(!checkbox2) }} />
                            <span className="checkmark"></span>
                          </label>
                          <Link to="" onClick={(e) => e.preventDefault()} className="color-box"><img alt=" " src={require("./assets/img/noun-color.svg")} /></Link>
                          <div className="input-container">
                            <div className="inner-input">
                              <input type="text" value="000000" onChange={() => console.log('object')} />
                            </div>
                            <div className="input-bottom-text dropdown-container"> 
                              <span>HEX </span>
                              <i className="fa fa-angle-down arrow-down" aria-hidden="true"></i>
                              <ul className="dropdown-item">
                                <li><Link to="" onClick={(e) => e.preventDefault()} >RGBA</Link></li>
                                <li><Link to="" onClick={(e) => e.preventDefault()} >HSLA</Link></li>
                              </ul>
                            </div>
                          </div>
                          <div className="input-container">
                            <div className="inner-input percent-value">
                              <input type="text" value="100%" onChange={() => console.log('object')} />
                            </div>
                            <span className="input-bottom-text">Opacity</span>
                          </div>
                          <div className="input-container">
                            <div className="inner-input border-value">
                              <input type="text" value="1" onChange={() => console.log('object')} />
                            </div>
                          </div>

                        </div>
                      </div>
                      : null}
                  </div>
                </div> : ''
              }




            </div>
          </div>
        </div>
        <div className="rectangle-footer">
          <label className="check-box">Apply styles to all confirmation pop ups
                <input type="checkbox" checked={checkbox3} onChange={() => { setCheckbox3(!checkbox3) }} />
            <span className="checkmark"></span>
          </label>

          <button type="btn" className="apply-btn">
            <span>Apply</span>
          </button>
        </div>
      </div>
    </div>
  );
}

export default App;
